<a name="1.0.1"></a>
## [1.0.1](https://github.com/mindslab-ai/brain-ta-train/compare/v1.0.0...v1.0.1)(2017-12-22)

### Bug Fixes

* **proto**: grpc 모듈 업데이트에 따른 패키지 이름 변경, cltrain
* **cltrain**: MLT 학습시 theaon 에러로 인한 버전 고정, 0.9.0으로

### Features

None

### Enhancements

None

<a name="1.0"></a>
# [1.0.0](https://github.com/mindslab-ai/brain-ta-train/compare/0971086...v1.0.0)(2017-12-08)
<!---
 MAJOR.MINOR 패치는 # 즉, H1으로 표시
  MAJOR>MINOR.PATCH는 ## 즉, H2로 표시한다.
--->

### Bug Fixes

  None

### Features

  * **proto**: `maum.brain.cl.train` 네임스페이스에 해당 사항 정리하여 사용하도록 처리
  * **classifier-trainer**: 실행엔진과 학습엔진의 분리에 따라서 [brain-ta-train](https://github.com/mindslab-ai/brain-ta-train)으로 이동

### Enhancements

  None
