Build
=====

From PyPI
~~~~~~~~~

::
pip install grpcio
pip install grpcio-tools


Prepare protobuf or grpc files
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

::
  python -m grpc.tools.protoc  --python_out=. --grpc_python_out=. \
  -I /usr/local/include -I ~/git/brain-cl-train/proto \
  ~/git/brain-cl-train/proto/maum/brain/cl/train/cltrainer.proto

Install
=======
