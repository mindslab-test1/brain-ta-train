# -*- coding: utf-8 -*-

import cPickle
import gzip
import os
import sys
from collections import OrderedDict

import numpy
import theano
from theano import tensor as T

import myutil


class GRU_encoder(object):
    ''' Gated Recurrent Unit RNN based encoder + MLP model '''

    def build_param(self, hyper_param, word2idx_dic, label2idx_dic):
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        # parameters of the model
        embedding = self.load_embedding(hyper_param['emb_file'], word2idx_dic, ne, de)
        self.emb = theano.shared(name='emb', value=embedding)
        # Wg : Wz, Wr
        self.Wgx = theano.shared(name='Wgx', value=0.01 * numpy.random.randn(de, 2 * nh).astype(theano.config.floatX))
        self.Whx = theano.shared(name='Whx', value=0.01 * numpy.random.randn(de, nh).astype(theano.config.floatX))
        # Ug : Uz, Ur
        identity1 = self.identity_weight(nh, nh)
        identity2 = self.identity_weight(nh, 2 * nh)
        self.Ugh = theano.shared(name='Ugh', value=identity2.astype(theano.config.floatX))
        self.Uhh = theano.shared(name='Uhh', value=identity1.astype(theano.config.floatX))
        # bg : bz, br
        self.bg = theano.shared(name='bg', value=numpy.zeros(2 * nh, dtype=theano.config.floatX))
        self.bh = theano.shared(name='bh', value=numpy.zeros(nh, dtype=theano.config.floatX))
        # others
        self.Wyc = theano.shared(name='Wyc', value=0.01 * numpy.random.randn(nh, nc).astype(theano.config.floatX))
        self.by = theano.shared(name='by', value=numpy.zeros(nc, dtype=theano.config.floatX))
        self.h0 = theano.shared(name='h0', value=numpy.zeros(nh, dtype=theano.config.floatX))

    def load_param(self, hyper_param, model_name):
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        print 'loading previous model:', model_name, '...',
        f = gzip.open(model_name, 'rb')
        [numpy_names, numpy_params] = cPickle.load(f)
        f.close()
        for numpy_param, name in zip(numpy_params, numpy_names):
            print name,
            if name == 'emb':
                self.emb = theano.shared(name=name, value=numpy_param)
            elif name == 'Wgx':
                self.Wgx = theano.shared(name=name, value=numpy_param)
            elif name == 'Whx':
                self.Whx = theano.shared(name=name, value=numpy_param)
            elif name == 'Ugh':
                self.Ugh = theano.shared(name=name, value=numpy_param)
            elif name == 'Uhh':
                self.Uhh = theano.shared(name=name, value=numpy_param)
            elif name == 'Wyc':
                self.Wyc = theano.shared(name=name, value=numpy_param)
            elif name == 'bg':
                self.bg = theano.shared(name=name, value=numpy_param)
            elif name == 'bh':
                self.bh = theano.shared(name=name, value=numpy_param)
            elif name == 'by':
                self.by = theano.shared(name=name, value=numpy_param)
            elif name == 'h0':
                self.h0 = theano.shared(name=name, value=numpy_param)
            else:
                print 'skip:', name

    def __init__(self, hyper_param, word2idx_dic, label2idx_dic):
        '''
        nh :: dimension of the hidden layer
        nc :: number of classes
        ne :: number of word embeddings in the vocabulary
        de :: dimension of the word embeddings
        nf :: number of feature
        nfe:: number of feature embeddings in the vocabulary - by leeck
        dfe:: dimension of the feature embeddings - by leeck
        cs :: word window context size
        emb_file :: word embedding file
        weight_decay :: weight decay
        dropout_rate :: dropout rate
        activation :: activation function: simg, tanh, relu
        word2idx_dic :: word to index dictionary
        label2idx_dic :: label to index dictionary
        '''
        self.hyper_param = hyper_param
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        weight_decay = hyper_param['weight_decay']
        dropout_rate = hyper_param['dropout_rate']
        activation = hyper_param['activation']
        learning_method = hyper_param['learning_method']
        # parameters of the model
        if hyper_param['load_model'] != '':
            self.load_param(hyper_param, hyper_param['load_model'])
        else:
            self.build_param(hyper_param, word2idx_dic, label2idx_dic)

        # parameters
        self.params = [self.emb, self.Wgx, self.Whx, self.Ugh, \
                       self.Uhh, self.Wyc, self.bg, self.bh, self.by, self.h0]

        if hyper_param['fixed_emb']:
            print 'fixed embeddig.'
            self.params.remove(self.emb)

        # as many lines as words in the sentence
        x_sentence = T.ivector('x_sentence')
        x_org = self.emb[x_sentence].reshape((x_sentence.shape[0], de))
        x = x_org[:-1]  # remove '</s>'

        y = T.iscalar('y')  # labels

        # for scan
        def step(x_t, h_tm1):
            # print 'z_t and r_t are combined!'
            all_t = T.nnet.sigmoid(T.dot(x_t, self.Wgx) + T.dot(h_tm1, self.Ugh) + self.bg)
            z_t = myutil.slice(all_t, 0, nh)
            r_t = myutil.slice(all_t, 1, nh)
            # candidate h_t
            ch_t = myutil.activation(activation, T.dot(x_t, self.Whx) + T.dot(r_t * h_tm1, self.Uhh) + self.bh)
            h_t = (1.0 - z_t) * h_tm1 + z_t * ch_t
            return h_t

        # dropout: ex. [0, 0.2, 0.5]
        # input layer dropout
        print "Projection layer dropout:", dropout_rate[0]
        dropout_x = myutil.dropout_from_layer(x, dropout_rate[0])
        # forward recurrent
        h, _ = theano.scan(fn=step,
                           sequences=dropout_x,
                           outputs_info=self.h0,
                           n_steps=dropout_x.shape[0])
        c = h[-1]
        # hidden layer dropout
        print "Hidden layer dropout:", dropout_rate[1]
        dropout_c = myutil.dropout_from_layer(c, dropout_rate[1])
        # score
        dropout_s = T.dot(dropout_c, self.Wyc) + self.by
        dropout_p_y_given_x = myutil.mysoftmax(dropout_s)

        # scaled version (for prediction)
        scaled_x = x * (1.0 - dropout_rate[0])
        # forward recurrent
        h2, _ = theano.scan(fn=step,
                            sequences=scaled_x,
                            outputs_info=self.h0,
                            n_steps=scaled_x.shape[0])
        c2 = h2[-1]
        # hidden layer
        scaled_c = c2 * (1.0 - dropout_rate[1])
        # score
        s = T.dot(scaled_c, self.Wyc) + self.by
        p_y_given_x = myutil.mysoftmax(s)
        # prediction
        # y_pred = T.argmax(p_y_given_x, axis=1)
        y_pred = T.argmax(p_y_given_x)
        print 'y_pred:', y_pred.ndim
        max_prob = T.max(p_y_given_x)
        print 'max_prob:', max_prob.ndim
        test_nll = -T.log(p_y_given_x)[y]
        print 'test_nll:', test_nll.ndim

        # cost and gradients and learning rate
        lr = T.scalar('lr')  # for SGD

        # NLL + L2-norm
        nll = -T.log(dropout_p_y_given_x)[y]
        cost = nll
        for param in self.params:
            if param.name == 'emb':
                continue
            cost += weight_decay * T.sum(param ** 2)

        # SGD
        if hyper_param['gradient_clip']:
            # exclude param from gradient cliping
            exclude_params = [self.emb]
            sgd_updates = myutil.sgd_updates_with_clipping(self.params, cost, lr, 9, exclude_params)
        else:
            gradients = T.grad(cost, self.params)
            sgd_updates = OrderedDict((p, p - lr * g) for p, g in zip(self.params, gradients))
        # SGD + momentum
        if hyper_param['gradient_clip']:
            momentum_updates = myutil.sgd_updates_momentum_with_clipping(self.params, cost, lr, 0.9, 9, exclude_params)
        else:
            momentum_updates = myutil.sgd_updates_momentum(self.params, cost, lr, 0.9)
        # AdaDelta (lr --> rho = 0.95)
        adadelta_updates = myutil.sgd_updates_adadelta(self.params, cost, lr, 1e-6, 9)

        # theano functions to compile
        self.classify = theano.function(inputs=[x_sentence, y], outputs=[y_pred, test_nll])
        self.classify2 = theano.function(inputs=[x_sentence], outputs=[y_pred, max_prob])
        # SGD
        self.train_sgd = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=sgd_updates)
        # SGD with momentum
        self.train_momentum = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=momentum_updates)
        # AdaDelta
        self.train_adadelta = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=adadelta_updates)

    def train(self, x, y, learning_method, learning_rate):
        words = map(lambda x: numpy.asarray(x).astype('int32'), x)
        label = y
        # learning_method : sgd, momentum, adadelta
        if learning_method == 'sgd':
            [cost, nll] = self.train_sgd(words, label, learning_rate)
        elif learning_method == 'momentum':
            [cost, nll] = self.train_momentum(words, label, learning_rate)
        elif learning_method == 'adadelta':
            [cost, nll] = self.train_adadelta(words, label, learning_rate)
        return [cost, nll]

    def load_embedding(self, file_name, word2id_dic, ne, de):
        """
        Loads word vectors from word2vec embedding (key value1 value2 ...)
        """
        embedding = 0.01 * numpy.random.randn(ne, de).astype(theano.config.floatX)
        if file_name != '':
            print >> sys.stderr, 'load embedding:', file_name, '...',
            count = 0
            if '.txt' in file_name:
                wv = myutil.load_txt_vec(file_name, word2id_dic)
            else:
                wv = myutil.load_bin_vec(file_name, word2id_dic)
                # print >> sys.stderr, 'load_bin_vec:', len(wv)
            for w in wv:
                idx = word2id_dic[w]
                if idx < ne:
                    embedding[idx] = wv[w]
                    count += 1
                else:
                    print >> sys.stderr, 'Warning(load_embedding):', w, idx, ne
            print >> sys.stderr, 'done:', count
        else:
            print >> sys.stderr, 'randomly init. embedding.'
        return embedding

    def identity_weight(self, x_dim, y_dim):
        print >> sys.stderr, 'identity weight:', x_dim, y_dim, '...',
        weight = numpy.zeros((x_dim, y_dim)).astype(theano.config.floatX)
        for i in range(x_dim):
            for j in range(y_dim):
                if i == j or i % x_dim == j % x_dim or i % y_dim == j % y_dim:
                    weight[i, j] = 1.0
        print >> sys.stderr, 'done.'
        return weight

    def save(self, folder, model_name, epoch):
        nn_type = self.hyper_param['nn_type']
        activation = self.hyper_param['activation']
        nh = self.hyper_param['nhidden']
        nh2 = self.hyper_param['nhidden2']
        de = self.hyper_param['emb_dimension']
        dropout_rate = self.hyper_param['dropout_rate']
        wd = self.hyper_param['weight_decay']
        learning_method = self.hyper_param['learning_method']
        proj = self.hyper_param['train_data'].split('/')[-1].replace('.train.pkl.gz', '')

        numpy_names = []
        numpy_params = []
        for param in self.params:
            numpy_names.append(param.name)
            numpy_params.append(param.get_value(borrow=True))
        if ('DO' in nn_type or 'Stacked' in nn_type) and nh != nh2:
            model_name2 = model_name + '.%s.%s.h%d-%d.e%d.d%g-%g.wd%g.i%d' % (
                activation, learning_method, nh, nh2, de, dropout_rate[0], dropout_rate[1], wd, epoch) + '.pkl.gz'
        else:
            model_name2 = model_name + '.%s.%s.%s.h%d.e%d.d%g-%g.wd%g.i%d' % (
                proj, activation, learning_method, nh, de, dropout_rate[0], dropout_rate[1], wd, epoch) + '.pkl.gz'
        model_name3 = model_name + '.%s.%s.%s.h%d.e%d.d%g-%g.wd%g.' % (
            proj, activation, learning_method, nh, de, dropout_rate[0], dropout_rate[1], wd) + 'best.pkl.gz'
        # f = open(os.path.join(folder, model_name2), 'wb')
        f = gzip.open(os.path.join(folder, model_name2), 'wb', compresslevel=1)
        cPickle.dump([numpy_names, numpy_params], f)
        f.close()
        f = gzip.open(os.path.join(folder, model_name3), 'wv', compresslevel=1)
        cPickle.dump([numpy_names, numpy_params], f)
        best_f = f.name
        f.close()
        return best_f
        # self.savetxt(folder, model_name)

    def load(self, folder, model_name):
        f = gzip.open(os.path.join(folder, model_name), 'rb')
        [self.numpy_names, self.numpy_params] = cPickle.load(f)
        f.close()

    def savetxt(self, folder, model_name):
        if self.numpy_params != None:
            for param, name in zip(self.numpy_params, self.numpy_names):
                numpy.savetxt(os.path.join(folder, model_name + '.' + name + '.txt'), param)
        else:
            for param in self.params:
                numpy.savetxt(os.path.join(folder, model_name + '.' + param.name + '.txt'), param.get_value())
