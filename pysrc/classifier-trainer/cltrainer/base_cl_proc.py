# -*- coding:utf-8 -*-
import os
import shutil
import sys
import tarfile

import maum.brain.nlp.nlp_pb2 as nlp
from common.config import Config
from maum.brain.cl.train import cltrainer_pb2 as cltr
from model_pickle2txt import load_model, save_model_as_txt

class BaseClassifierTrainer:
    conf = Config()
    nlp_stub = None
    proc = None
    base = ''
    words_vocab = ''
    best_file = ''
    list_label = []
    helper = None

    """
    공통 DNN Classifier 학습기
    """
    def __init__(self, helper, proc):
        self.helper = helper
        self.proc = proc
        self.base = self.conf.get('brain-ta.cl.trainer.workspace.dir')
        if not os.path.isdir(self.base):
            os.makedirs(self.base)
        if self.base[-1] != '/':
            self.base += '/'

    def pickle_to_text(self):
        print self.output, self.best_file
        save_model_as_txt(self.output, load_model(self.best_file))

    @staticmethod
    def gen_input(category, lang):
        input_texts = []
        for txt in category.text_lemmas:
            in_txt = nlp.InputText()
            # check point #1 in server side
            temp_token = txt.text.split()
            if len(temp_token) == 1 and len(temp_token[0]) == 1:
                continue
            else:
                in_txt.text = txt.text
                in_txt.lang = lang
                in_txt.level = 1
                in_txt.split_sentence = True
            input_texts.append(in_txt)
        for txt in input_texts:
            yield txt

    def do_tagger(self, cl_model):
        """
        기존의 mk_pos_tagger 또는 eng_tagger 가 직접 파일을 열어서 수행하는 작업을
        대신 수행한다.
        :return: void
        """
        self.proc.step = cltr.CL_TRAIN_MAKE_MORPHEME
        self.helper.save_proc(self.proc)

        if cl_model.has_lemma == True:
            return
        for cat in cl_model.categories:
            res = self.nlp_stub.AnalyzeMultiple(
                    BaseClassifierTrainer.gen_input(cat, cl_model.lang))
            for idx, doc in enumerate(res):
                lemmas = []
                for sent in doc.sentences:
                    for morp in sent.morps:
                        lemmas.append(morp.lemma + '/' + morp.type)
                # check point 2 in server side
                temp_token = cat.text_lemmas[idx].text.split()
                if len(temp_token) == 1 and len(temp_token[0]) == 1:
                    continue
                else:
                    cat.text_lemmas[idx].lemma = ' '.join(lemmas)
                #print cat.text_lemmas[idx].lemma.encode('utf-8')
                #print cat.text_lemmas[idx].text.encode('utf-8')

                # 모든 lemma를 변경한 다음에는 point를 증가시켜 준다.
            self.proc.value += 1
            self.helper.save_proc(self.proc)

    def do_sa_data(self):
        return []

    def do_dnn(self):
        pass

    def encode_as(self, str):
        return str

    def _load_vocab_word(self):
        vocab_word = {}
        i = 0
        f = open(self.words_vocab)
        for line in f:
            line = line.replace('\r', '').replace('\n', '')
            word = line.split()
            if len(word) == 1 or len(word) == 2:
                vocab_word[word[0]] = i
            i += 1
        else:
            print >> sys.stderr, "Warning:", line
        print >> sys.stderr, "done:", i
        f.close()
        return vocab_word

    def _load_vocab_label(self, model):
        """
        모델 데이터에서 label만을 꺼내서 사전과 목록으로 만든다.

        :param model: 입력 모델 데이터
        :return: 레이블 사전, 레이블 목록
        """
        vocab_label = {}
        list_label = []

        cate_arr = []
        for cate in model.categories:
            print cate.labels
            s = '_'.join(cate.labels)
            l = self.encode_as(s)
            cate_arr.append(l)

        src_list = cate_arr[:]

        cate_arr.sort()
        print cate_arr

        i = 0
        for label in cate_arr:
            vocab_label[label] = i
            list_label.append(label)
            i += 1
        print >> sys.stderr, "done:", i
        return vocab_label, list_label, src_list

    def save_vocab_sa_txt(self, uuid):
        base = self.base
        data_path = base + str(uuid)
        file = os.path.join(data_path, 'vocab_sa.txt')
        self.vocab_sa = file
        f = open(file, 'w')
        for l in self.list_label:
            print >> f, l
        f.close()

    def save_as_tar(self, model, lang):
        self.proc.step = cltr.CL_TRAIN_FINAL
        self.helper.save_proc(self.proc)

        tar_file = self.helper.get_tar_file()
        work_dir = self.helper.get_tar_work_dir()

        print tar_file, work_dir

        for file in [self.output, self.vocab_sa]:
            name = os.path.basename(file)
            new_file = os.path.join(work_dir, name)
            print 'rename ', file, new_file
            os.rename(file, new_file)

        self.tar = tar_file
        tgz = tarfile.open(self.tar, 'w:gz')
        print 'TAR', self.tar, os.path.basename(work_dir)
        tgz.add(name=work_dir,
                arcname=os.path.basename(work_dir),
                recursive=True)
        tgz.close()
        self.proc.step = cltr.CL_TRAIN_FINAL
        self.proc.value += 3
        self.helper.save_proc(self.proc)

    def remove_workspace(self, uuid):
        self.proc.step = cltr.CL_TRAIN_CLEAN
        self.helper.save_proc(self.proc)

        data_path = self.helper.workspace_path()
        shutil.rmtree(data_path)
        os.remove(self.helper.get_model_file())
        self.proc.step = cltr.CL_TRAIN_CLEAN
        self.proc.value += 5
        self.helper.save_proc(self.proc)

        # os.remove(self.helper.get_proc_file())
        # proc file은 proxy에서 상태를 확인한 이후에 삭제하도록 한다
