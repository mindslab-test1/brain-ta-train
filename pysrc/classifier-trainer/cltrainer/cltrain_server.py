# -*- coding:utf-8 -*-
import os
import sys

import grpc
from google.protobuf.empty_pb2 import Empty

import maum.brain.cl.train.cltrainer_pb2 as cltr
from maum.brain.cl.train import cltrainer_pb2_grpc
from cltrainer.cl_trainer_proxy import ClassifierTrainerProxy
from common.config import Config


class ClassifierTrainer(cltrainer_pb2_grpc.ClassifierTrainerServicer):
    conf = Config()
    trainers = {}
    childs = {}
    # gpu round robin
    gpu_count = 0
    next_gpu = 0

    def _get_gpu_count(self):
        try:
            self.gpu_count = len(os.listdir('/proc/driver/nvidia/gpus'))
        except OSError, e:
            self.gpu_count = 0
            print 'No gpu exists'
            sys.exit(2)

    def _next_gpu_index(self):
        ret = self.next_gpu
        self.next_gpu += 1
        print "NEXT GPU", ret, self.next_gpu, 'count', self.gpu_count
        if self.next_gpu >= self.gpu_count:
            self.next_gpu = 0
        return ret

    """
    Provide methods that implement HmdClassifier
    """

    def __init__(self):
        self._get_gpu_count()
        pass

    def Open(self, cl_model, context):
        trainer = ClassifierTrainerProxy(self._next_gpu_index())
        trainer.open(cl_model)

        ret = cltr.ClTrainKey()
        ret.train_id = trainer.get_key()
        print 'NEW TRAINER ', trainer.get_key(), ret.train_id
        self.trainers[trainer.get_key()] = trainer
        self.childs[trainer.get_pid()] = trainer
        print self.childs
        print self.trainers
        return ret

    def GetProgress(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            return trainer.get_progress()
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def GetBinary(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[str(key.train_id)]
            return trainer.get_binary()
        else:
            return ClassifierTrainerProxy.get_binary_by_key(str(key.train_id),
                                                            context, grpc)

    def RemoveBinary(self, key, context):
        ClassifierTrainerProxy.remove_binary_by_key(str(key.train_id))
        return Empty()

    def Close(self, key, context):
        if key.train_id in self.trainers:
            trainer = self.trainers[key.train_id]
            ret = trainer.get_progress()
            del self.trainers[key.train_id]
            pid = self.childs.keys()[self.childs.values().index(trainer)]
            del self.childs[pid]
            print 'NOW delete proxy trainer', pid, key, trainer
            del trainer
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def Stop(self, key, context):
        id = str(key.train_id)
        trainer = self.trainers[id]
        if trainer != None:
            ret = trainer.get_progress()
            trainer.cancel()
            return ret
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    def GetAllProgress(self, empty, context):
        ret = cltr.ClTrainStatusList()
        res = []
        for tr in self.trainers.itervalues():
            res.append(tr.get_progress())
        ret.cl_trains.extend(res)
        return ret

    def update_proc(self, proc_stat):
        """
        child에서 종료할 때, 해당 상태를 정리한다.

        :param proc_stat: proc의 상태 값을 전달한다.
        """
        key = str(proc_stat.key)
        if key in self.trainers:
            trainer = self.trainers[key]
            pid = self.childs.keys()[self.childs.values().index(trainer)]
            assert pid == proc_stat.pid
            del self.trainers[key]
            del self.childs[pid]
            # 상태를 기록해준다.
            trainer.set_result(proc_stat.result)
            print 'NOW delete proxy trainer', pid, key, trainer
            del trainer
