# -*- coding:utf-8 -*-
import glob
import json
import os
import resource
import signal
import requests
from uuid import uuid4

from google.protobuf.timestamp_pb2 import Timestamp

from cl_trainer_helper import TrainerHelper
from common.config import Config
from maum.brain.cl.train import cltrainer_pb2 as cltr

_TUTOR_API_KEY = '7c3a778e29ac522de62c4e22aa90c9'


class ClassifierTrainerProxy:
    """
    공통 DNN Classifier Proxy
    """
    uuid = None
    pid = None
    helper = None
    callback_url = None
    fake_cnt = 0
    cl_model = cltr.ClModel()
    train_result = cltr.training
    started = Timestamp()
    gpu_idx = 0

    def __init__(self, gpu_no):
        self.dic = {}
        self.uuid = uuid4()
        self.key = str(self.uuid)
        self.gpu_idx = gpu_no
        self.model = None

    def __del__(self):
        if self.key != None:
            self.finish_hook()
            self.key = None

    def run_trainer(self, cl_model):
        self.helper.save_model(cl_model)
        cmd, args = self.helper.child_command_line()
        env = os.environ.copy()
        env['PATH'] = env['PATH'] + ':/usr/local/cuda/bin'
        t_flags = 'mode=FAST_RUN,floatX=float32,device=gpu'
        t_flags += str(self.gpu_idx)
        env['THEANO_FLAGS'] = t_flags
        print 'before exec:', cmd, args, env
        try:
            os.execve(cmd, args, env)
        except OSError, e:
            print 'execve failed', cmd, args, env, e.errno, e.strerror

    def open(self, cl_model):
        self.callback_url = cl_model.callback_url
        self.model = cl_model.model
        """
        실지로 실행하는 학습 프로세스를 실행한다.
        학습 프로세스에서는 학습 실행 결과를 주기적으로 파일에 기록한다.
        빠를 실행 속도를 보장하기 위해서 위와 같이 처리한다.
        파일에 기록할 때 FILE LOCK을 실행한다.
        :param cl_model:
        :return:
        """
        self.helper = TrainerHelper(self.uuid, cl_model.lang, cl_model.model)
        newpid = os.fork()
        if newpid == 0:
            (cur, max) = resource.getrlimit(resource.RLIMIT_NOFILE)
            try:
                for fd in range(3, cur):
                    os.close(fd)
            except OSError, e:
                pass
            self.run_trainer(cl_model)
        else:
            self.pid = newpid
            print 'fork parent:', self.pid, newpid

    def finish_hook(self):
        # 파일을 삭제한다.        self.helper.remove_proc()
        res = cltr.ClTrainResult.Name(self.train_result)
        print 'finish_hook', res, self.train_result
        url = self.callback_url + _TUTOR_API_KEY
        print 'call back url: ', url

        cb_msg = {}
        cb_msg['message'] = 'Classifier train job ' + self.key + \
                            ', result:' + res
        cb_msg['data'] = {
            'key': self.key,
            'name': self.model,
            'result': res,
            'binary': self.helper.get_tar_filename()
        }

        body = json.dumps(cb_msg)
        req = requests.post(url, data=body,
                            headers={'Content-Type': 'application/json'})

        print 'RESULT : ', req.text

    def get_key(self):
        return self.key

    def get_pid(self):
        return self.pid

    def get_progress(self):
        proc = self.helper.load_proc()
        try:
            self.train_result = proc.result
        except AttributeError, e:
            print 'can not load proc file.', e.message
        finally:
            # if self.train_result != cltr.training:
            #     self.helper.remove_proc()
            return proc

    def get_binary(self):
        tar = self.helper.get_tar_file()
        if os.path.exists(tar):
            return self.bytes_from_file(tar)
        else:
            return None

    @staticmethod
    def bytes_from_file(filename, chunksize=1024 * 1024):
        with open(filename, "rb") as f:
            while True:
                chunk = f.read(chunksize)
                if chunk:
                    tar = cltr.ClTrainBinary()
                    tar.tar = chunk
                    yield tar
                else:
                    break

    @staticmethod
    def get_binary_by_key(uuid, context, grpc):
        conf = Config()
        ws = conf.get('brain-ta.cl.trainer.workspace.dir')
        find = ws + '/*' + uuid + '.climage.tar.gz'
        files = glob.glob(find)
        if len(files) > 0:
            return ClassifierTrainerProxy.bytes_from_file(filename=files[0])
        else:
            context.set_code(grpc.StatusCode.NOT_FOUND)
            context.set_details('train id not found')
            raise grpc.RpcError("not found")

    @staticmethod
    def remove_binary_by_key(uuid):
        conf = Config()
        ws = conf.get('brain-ta.cl.trainer.workspace.dir')
        find = ws + '/*' + uuid + '.climage.tar.gz'
        files = glob.glob(find)
        for f in files:
            print 'remove binary by key ', uuid, f
            os.remove(f)

    def cancel(self):
        proc = self.helper.load_proc()
        if proc.result == cltr.training:
            os.kill(self.pid, signal.SIGTERM)
            self.train_result = cltr.cancelled
            proc.result = cltr.cancelled
            self.helper.save_proc(proc)

    def set_result(self, res):
        print 'SET RESULT', cltr.ClTrainResult.Name(res)
        self.train_result = res
