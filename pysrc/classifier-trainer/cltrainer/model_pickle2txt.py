# -*- coding: utf-8 -*-
"""
author : Kyoungho Choi <gtraccoon@gmail.com>
"""

__author__ = 'Kyoungho Choi'
__version__ = '2015-9-9'

import cPickle
import gzip

def load(model_name):
    f = gzip.open(model_name, 'rb')
    train_set, valid_set, test_set, dic = cPickle.load(f)
    f.close()
    return test_set


def load_model(model_name):
    f = gzip.open(model_name, 'rb')
    tmpvalue = cPickle.load(f)
    f.close()
    return tmpvalue


def save_model_as_txt(model_name, model):
    f = open(model_name, 'w')
    for i in range(len(model[0])):
        f.write(model[0][i] + '\n')
        try:
            print model[0][i], len(model[1][i]), len(model[1][i][0])
            for sent in model[1][i]:
                f.write(str(sent[0]))
                for word in sent[1:]:
                    f.write('\t' + str(word))
                f.write('\n')
            f.write('\n')
        except:
            for sent in model[1][i]:
                f.write(str(sent))
                f.write('\n')
            f.write('\n')
    f.close()
