#!/usr/bin/env python
# -*- coding:utf-8 -*-

import os
import signal
import sys
import traceback

import grpc

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

import argparse
from uuid import UUID

from common.config import Config
from maum.common import lang_pb2
from maum.brain.cl.train import cltrainer_pb2 as cltr
from maum.brain.cl.train import cltrainer_pb2_grpc
from cltrainer.eng.english_cl_trainer import EnglishClassifierTrainer
from cltrainer.kor.korean_cl_trainer import KoreanClassifierTrainer
from cltrainer.cl_trainer_helper import TrainerHelper

import theano

__cl_proc__ = None


def _get_gpu_count():
    return len(os.listdir('/proc/driver/nvidia/gpus'))


class CltrainProcess:
    uuid = None
    proc = None
    lang_code = None
    model = None
    proc = None

    def report_stat(self, result, sig_no):
        parent_remote = self.helper.get_parent_endpoint()
        print 'REPORT STAT', parent_remote
        chan = grpc.insecure_channel(parent_remote)
        stub = cltrainer_pb2_grpc.TrainerMonitorStub(chan)

        proc_stat = cltr.TrainProcStatus()
        proc_stat.key = str(self.uuid)
        proc_stat.pid = os.getpid()
        proc_stat.result = result
        proc_stat.sig_no = sig_no
        stub.Notify(proc_stat)

    def _handle_signal(self, signal, frame):
        self.report_stat(cltr.cancelled, signal)
        sys.exit(0)

    def __init__(self, uuid, lang, model):
        """
        :param uuid: 문자열 uuid
        :param lang: lang (kor, eng)
        """
        signal.signal(signal.SIGTERM, self._handle_signal)
        signal.signal(signal.SIGSEGV, self._handle_signal)
        signal.signal(signal.SIGINT, self._handle_signal)
        signal.signal(signal.SIGBUS, self._handle_signal)

        self.uuid = UUID(uuid)
        self.lang_code = lang_pb2.LangCode.Value(lang)

        self.helper = TrainerHelper(self.uuid, self.lang_code, model)
        self.model = self.helper.load_model()
        self.init_proc()
        #print self.model

    def calc_model_maximum(self):
        model = self.model
        point = 0
        # lemma
        if not model.has_lemma:
            point += len(model.categories)
        # sa
        point += len(model.categories)
        # pickle
        point += 4
        # DNN
        point += (model.node_count * model.run_count)
        # final
        point += 3
        # clear
        point += 5
        return point

    def init_proc(self):
        proc = cltr.ClTrainStatus()

        proc.result = cltr.training
        proc.key = str(self.uuid)
        proc.step = cltr.CL_TRAIN_START
        proc.model = self.model.model
        proc.lang = self.model.lang
        proc.has_lemma = self.model.has_lemma
        proc.maximum = self.calc_model_maximum()
        proc.value = 0

        proc.node_count = self.model.node_count
        proc.run_count = self.model.run_count
        proc.run_cur = 0
        dev = theano.config.device
        proc.assigned_gpu_idx = int(dev[3:])
        proc.gpu_dev_name = dev
        proc.gpu_count = _get_gpu_count()
        proc.gpu_model_name = ''

        proc.started.GetCurrentTime()
        proc.elapsed.seconds = 0
        proc.elapsed.nanos = 0

        self.proc = proc
        self.helper.save_proc(self.proc)

    def train(self):
        """
        main train 함수
        :return:
        """
        trainer = None
        if self.model.lang == lang_pb2.kor:
            trainer = KoreanClassifierTrainer(self.helper, self.proc)
        elif self.model.lang == lang_pb2.eng:
            trainer = EnglishClassifierTrainer(self.helper, self.proc)
        else:
            # set failed
            return None

        if not self.model.has_lemma:
            print ' TRAIN :', self.uuid,  'do tagger ------------'
            trainer.do_tagger(self.model)

        try:
            print ' TRAIN :', self.uuid,  'do sa data ------------'
            trainer.do_sa_data(self.uuid, self.model)
            print ' TRAIN :', self.uuid,  'do dnn ------------'
            trainer.do_dnn(self.uuid, self.model)
            print ' TRAIN :', self.uuid,  'pickle to text'
            trainer.pickle_to_text()
            trainer.save_vocab_sa_txt(self.uuid)
            print ' TRAIN :', self.uuid,  'save as tar'
            trainer.save_as_tar(self.model.model,
                                lang_pb2.LangCode.Name(self.model.lang))
            print ' TRAIN :', self.uuid,  'clean'
            trainer.remove_workspace(self.uuid)
            print ' TRAIN :', self.uuid,  'NOTIFY'
            # report success
            self.proc.step = cltr.CL_TRAIN_DONE
            self.helper.save_proc(self.proc)
            self.report_stat(cltr.success, 0)
        except:
            print '==='
            print sys.exc_type
            traceback.print_exc()
            self.report_stat(cltr.failed, 0)

if __name__ == '__main__':
    print 'Starting child with ', sys.argv
    conf = Config()
    conf.init('brain-ta-train.conf')

    parser = argparse.ArgumentParser(
        description='classifier trainer executor')
    parser.add_argument('-u', '--uuid',
                        nargs='?',
                        dest='uuid',
                        required=True,
                        help='Unique trainer id')
    parser.add_argument('-l', '--lang',
                        nargs='?',
                        dest='lang',
                        required=True,
                        help='Language (kor, eng)')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='Model Name')

    args = parser.parse_args()

    #print args.uuid, args.lang, args.model

    # TODO signal handling
    # kill signal stop 처리

    cl_proc = CltrainProcess(args.uuid, args.lang, args.model)
    __cl_proc__ = cl_proc
    cl_proc.train()
