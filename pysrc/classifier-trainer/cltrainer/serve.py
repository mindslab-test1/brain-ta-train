import time

import grpc
from concurrent import futures

from cltrain_monitor import ClTrainerMonitor
from cltrain_server import ClassifierTrainer
from common.config import Config
from maum.brain.cl.train import cltrainer_pb2 as cltr
from maum.brain.cl.train import cltrainer_pb2_grpc

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

def serve():
    conf = Config()
    conf.init('brain-ta-train.conf')

    svc = ClassifierTrainer()
    mon = ClTrainerMonitor(svc)
    data = [
        ('grpc.max_connection_idle_ms', int(conf.get("brain-ta.cl.trainer.front.timeout"))),
        ('grpc.max_connection_age_ms', int(conf.get("brain-ta.cl.trainer.front.timeout"))),
	      ('grpc.max_send_message_length', 64 * 1024 * 1024),
	      ('grpc.max_receive_message_length', 64 * 1024 * 1024)
    ]

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10), options=data)
    cltrainer_pb2_grpc.add_ClassifierTrainerServicer_to_server(svc, server)
    cltrainer_pb2_grpc.add_TrainerMonitorServicer_to_server(mon, server)

    port = conf.get('brain-ta.cl.trainer.front.port')
    server.add_insecure_port('[::]:' + port)

    server.start()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
