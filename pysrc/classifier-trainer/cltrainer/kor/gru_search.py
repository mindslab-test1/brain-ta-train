# -*- coding: utf-8 -*-

import cPickle
import gzip
from collections import OrderedDict

import numpy
import theano
from theano import tensor as T

import gru_encoder
from cltrainer.kor import myutil


class GRU_search(gru_encoder.GRU_encoder):
    ''' Gated Recurrent Unit RNN based search + MLP model '''
    def build_param(self, hyper_param, word2idx_dic, label2idx_dic):
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        # parameters of the model
        embedding = self.load_embedding(hyper_param['emb_file'], word2idx_dic, ne, de)
        self.emb = theano.shared(name='emb', value=embedding)
        # Wg : Wz, Wr
        self.Wgx = theano.shared(name='Wgx', value=0.01 * numpy.random.randn(de, 2*nh).astype(theano.config.floatX))
        self.Wgxb = theano.shared(name='Wgxb', value=0.01 * numpy.random.randn(de, 2*nh).astype(theano.config.floatX))
        self.Whx = theano.shared(name='Whx', value=0.01 * numpy.random.randn(de, nh).astype(theano.config.floatX))
        self.Whxb = theano.shared(name='Whxb', value=0.01 * numpy.random.randn(de, nh).astype(theano.config.floatX))
        self.Whc = theano.shared(name='Whc', value=0.01 * numpy.random.randn(2*nh, nh).astype(theano.config.floatX))
        self.Wh0c = theano.shared(name='Wh0c', value=0.01 * numpy.random.randn(2*nh, nh).astype(theano.config.floatX))
        # Ug : Uz, Ur
        identity1 = self.identity_weight(nh, nh)
        identity2 = self.identity_weight(nh, 2*nh)
        self.Ugh = theano.shared(name='Ugh', value=identity2.astype(theano.config.floatX))
        self.Ughb = theano.shared(name='Ughb', value=identity2.astype(theano.config.floatX))
        self.Uhh = theano.shared(name='Uhh', value=identity1.astype(theano.config.floatX))
        self.Uhhb = theano.shared(name='Uhhb', value=identity1.astype(theano.config.floatX))
        # bg : bz, br
        self.bg = theano.shared(name='bg', value=numpy.zeros(2*nh, dtype=theano.config.floatX))
        self.bgb = theano.shared(name='bgb', value=numpy.zeros(2*nh, dtype=theano.config.floatX))
        self.bh = theano.shared(name='bh', value=numpy.zeros(nh, dtype=theano.config.floatX))
        self.bhb = theano.shared(name='bhb', value=numpy.zeros(nh, dtype=theano.config.floatX))
        # aligment
        self.Wa = theano.shared(name='Wa', value=0.01 * numpy.random.randn(2*nh, nh).astype(theano.config.floatX))
        self.ba = theano.shared(name='ba', value=numpy.zeros(nh, dtype=theano.config.floatX))
        self.va = theano.shared(name='va', value=numpy.zeros(nh, dtype=theano.config.floatX))
        # others
        self.Wyc = theano.shared(name='Wyc', value=0.01 * numpy.random.randn(2*nh, nc).astype(theano.config.floatX))
        self.by = theano.shared(name='by', value=numpy.zeros(nc, dtype=theano.config.floatX))
        self.h0 = theano.shared(name='h0', value=numpy.zeros(nh, dtype=theano.config.floatX))
        self.h0b = theano.shared(name='h0b', value=numpy.zeros(nh, dtype=theano.config.floatX))

    def load_param(self, hyper_param, model_name):
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        print 'loading previous model:', model_name, '...',
        f = gzip.open(model_name, 'rb')
        [numpy_names, numpy_params] = cPickle.load(f)
        f.close()
        for numpy_param, name in zip(numpy_params, numpy_names):
            print name,
            if name == 'emb': self.emb = theano.shared(name=name, value=numpy_param)
            # Wg : Wz, Wr
            elif name == 'Wgx': self.Wgx = theano.shared(name=name, value=numpy_param)
            elif name == 'Wgxb': self.Wgxb = theano.shared(name=name, value=numpy_param)
            elif name == 'Whx': self.Whx = theano.shared(name=name, value=numpy_param)
            elif name == 'Whxb': self.Whxb = theano.shared(name=name, value=numpy_param)
            # Ug : Uz, Ur
            elif name == 'Ugh': self.Ugh = theano.shared(name=name, value=numpy_param)
            elif name == 'Ughb': self.Ughb = theano.shared(name=name, value=numpy_param)
            elif name == 'Uhh': self.Uhh = theano.shared(name=name, value=numpy_param)
            elif name == 'Uhhb': self.Uhhb = theano.shared(name=name, value=numpy_param)
            # bg : bz, br
            elif name == 'bg': self.bg = theano.shared(name=name, value=numpy_param)
            elif name == 'bgb': self.bgb = theano.shared(name=name, value=numpy_param)
            elif name == 'bh': self.bh = theano.shared(name=name, value=numpy_param)
            elif name == 'bhb': self.bhb = theano.shared(name=name, value=numpy_param)
            # aligment
            elif name == 'Wa': self.Wa = theano.shared(name=name, value=numpy_param)
            elif name == 'ba': self.ba = theano.shared(name=name, value=numpy_param)
            elif name == 'va': self.va = theano.shared(name=name, value=numpy_param)
            # others
            elif name == 'Wyc': self.Wyc = theano.shared(name=name, value=numpy_param)
            elif name == 'Wyy': self.Wyy = theano.shared(name=name, value=numpy_param)
            elif name == 'by': self.by = theano.shared(name=name, value=numpy_param)
            elif name == 'h0': self.h0 = theano.shared(name=name, value=numpy_param)
            elif name == 'h0b': self.h0b = theano.shared(name=name, value=numpy_param)
            else: print '\nskip:', name
        print 'done.'

    def __init__(self, hyper_param, word2idx_dic, label2idx_dic):
        '''
        nh :: dimension of the hidden layer
        nc :: number of classes
        ne :: number of word embeddings in the vocabulary
        de :: dimension of the word embeddings
        nf :: number of feature
        nfe:: number of feature embeddings in the vocabulary - by leeck
        dfe:: dimension of the feature embeddings - by leeck
        cs :: word window context size
        emb_file :: word embedding file
        weight_decay :: weight decay
        dropout_rate :: dropout rate
        activation :: activation function: simg, tanh, relu
        word2idx_dic :: word to index dictionary
        label2idx_dic :: label to index dictionary
        '''
        self.hyper_param = hyper_param
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        weight_decay = hyper_param['weight_decay']
        dropout_rate = hyper_param['dropout_rate']
        activation = hyper_param['activation']
        learning_method = hyper_param['learning_method']
        # parameters of the model
        if hyper_param['load_model'] != '':
            self.load_param(hyper_param, hyper_param['load_model'])
        else:
            self.build_param(hyper_param, word2idx_dic, label2idx_dic)

        # parameters
        self.params = [self.emb, self.Wgx, self.Wgxb, self.Whx, self.Whxb, \
                self.Ugh, self.Ughb, self.Uhh, self.Uhhb, \
                self.bg, self.bgb, self.bh, self.bhb, \
                self.Wa, self.ba, self.va, \
                self.Wyc, self.by, \
                self.h0, self.h0b]

        if hyper_param['fixed_emb']:
            print 'fixed embeddig.'
            self.params.remove(self.emb)

        # as many lines as words in the sentence
        x_sentence = T.ivector('x_sentence')
        x = self.emb[x_sentence].reshape((x_sentence.shape[0], de)) # don't remove '</s>'
        x_reverse = x[::-1] # reverse for backward

        y = T.iscalar('y')  # labels

        # for scan
        #def step(x_t, h_tm1):
        def step(dot_x_t_Wgx, dot_x_t_Whx, h_tm1):
            #print 'z_t, r_t are combined!'
            #all_t = T.nnet.sigmoid(T.dot(x_t, self.Wgx) + T.dot(h_tm1, self.Ugh) + self.bg)
            all_t = T.nnet.sigmoid(dot_x_t_Wgx + T.dot(h_tm1, self.Ugh))
            z_t = myutil.slice(all_t, 0, nh)
            r_t = myutil.slice(all_t, 1, nh)
            # candidate h_t
            #ch_t = myutil.activation(activation, T.dot(x_t, self.Whx) + T.dot(r_t * h_tm1, self.Uhh) + self.bh)
            ch_t = myutil.activation(activation, dot_x_t_Whx + T.dot(r_t * h_tm1, self.Uhh))
            h_t = (1.0 - z_t) * h_tm1 + z_t * ch_t
            return h_t

        #def backward_step(x_t, h_tm1):
        def backward_step(dot_x_t_Wgxb, dot_x_t_Whxb, h_tm1):
            #print 'z_t and r_t are combined!'
            #all_t = T.nnet.sigmoid(T.dot(x_t, self.Wgxb) + T.dot(h_tm1, self.Ughb) + self.bgb)
            all_t = T.nnet.sigmoid(dot_x_t_Wgxb + T.dot(h_tm1, self.Ughb))
            z_t = myutil.slice(all_t, 0, nh)
            r_t = myutil.slice(all_t, 1, nh)
            # candidate h_t
            #ch_t = myutil.activation(activation, T.dot(x_t, self.Whxb) + T.dot(r_t * h_tm1, self.Uhhb) + self.bhb)
            ch_t = myutil.activation(activation, dot_x_t_Whxb + T.dot(r_t * h_tm1, self.Uhhb))
            h_t = (1.0 - z_t) * h_tm1 + z_t * ch_t
            return h_t

        ## dropout: ex. [0, 0.2, 0.5]
        # input layer dropout
        print "Projection layer dropout:", dropout_rate[0]
        dropout_x = myutil.dropout_from_layer(x, dropout_rate[0])
        dropout_x_reverse = dropout_x[::-1] # reverse for backward
        dot_drop_x_Wgx = T.dot(dropout_x, self.Wgx) + self.bg
        dot_drop_x_Whx = T.dot(dropout_x, self.Whx) + self.bh
        dot_drop_x_rev_Wgx = T.dot(dropout_x_reverse, self.Wgxb) + self.bgb
        dot_drop_x_rev_Whx = T.dot(dropout_x_reverse, self.Whxb) + self.bhb
        # forward recurrent
        hf, _ = theano.scan(fn=step,
                sequences=[dot_drop_x_Wgx, dot_drop_x_Whx],
                outputs_info=self.h0,
                n_steps=dropout_x.shape[0])
        # backward recurrent
        hb_reverse, _ = theano.scan(fn=backward_step,
                sequences=[dot_drop_x_rev_Wgx, dot_drop_x_rev_Whx],
                outputs_info=self.h0b,
                n_steps=dropout_x_reverse.shape[0])
        hb = hb_reverse[::-1]
        h = T.concatenate([hf, hb], axis=1)
        # attention: search c
        # z : h (n_steps * 2nh) * Wa (2nh * nh) -> n_steps * nh
        z = T.tanh(T.dot(h, self.Wa) + self.ba)
        print 'z:', z.ndim
        # e : va (nh) * z.T (nh * n_steps) -> n_steps
        e = T.dot(self.va, z.T)
        print 'e:', e.ndim
        max_e = T.max(e)
        exp_e = T.exp(e - max_e)
        a = exp_e / exp_e.sum()
        print 'a:', a.ndim
        # c : a (n_steps) * h (n_steps * 2nh) -> 2nh
        c = T.dot(a, h)
        print 'c:', c.ndim
        # hidden layer dropout
        print "Hidden layer dropout:", dropout_rate[1]
        dropout_c = myutil.dropout_from_layer(c, dropout_rate[1])
        # score
        dropout_s = T.dot(dropout_c, self.Wyc) + self.by
        dropout_p_y_given_x = myutil.mysoftmax(dropout_s)

        ## scaled version (for prediction)
        scaled_x = x * (1.0 - dropout_rate[0])
        scaled_x_reverse = scaled_x[::-1] # reverse for backward
        dot_x_Wgx = T.dot(scaled_x, self.Wgx) + self.bg
        dot_x_Whx = T.dot(scaled_x, self.Whx) + self.bh
        dot_x_rev_Wgx = T.dot(scaled_x_reverse, self.Wgxb) + self.bgb
        dot_x_rev_Whx = T.dot(scaled_x_reverse, self.Whxb) + self.bhb
        # forward recurrent
        hf2, _ = theano.scan(fn=step,
                sequences=[dot_x_Wgx, dot_x_Whx],
                outputs_info=self.h0,
                n_steps=scaled_x.shape[0])
        # backward recurrent
        hb2_reverse, _ = theano.scan(fn=backward_step,
                sequences=[dot_x_rev_Wgx, dot_x_rev_Whx],
                outputs_info=self.h0b,
                n_steps=scaled_x_reverse.shape[0])
        hb2 = hb2_reverse[::-1]
        h2 = T.concatenate([hf2, hb2], axis=1)
        # attention: search c
        # z : h (n_steps * 2nh) * Wa (2nh * nh) -> n_steps * nh
        z2 = T.tanh(T.dot(h2, self.Wa) + self.ba)
        # e : va (nh) * z.T (nh * n_steps) -> n_steps
        e2 = T.dot(self.va, z2.T)
        max_e2 = T.max(e2)
        exp_e2 = T.exp(e2 - max_e2)
        a2 = exp_e2 / exp_e2.sum()
        # c : a (n_steps) * h (n_steps * 2nh) -> 2nh
        c2 = T.dot(a2, h2)
        print 'c2:', c.ndim
        # hidden layer
        scaled_c = c2 * (1.0 - dropout_rate[1])
        # score
        s = T.dot(scaled_c, self.Wyc) + self.by
        p_y_given_x = myutil.mysoftmax(s)
        # prediction
        #y_pred = T.argmax(p_y_given_x, axis=1)
        y_pred = T.argmax(p_y_given_x)
        test_nll = -T.log(p_y_given_x)[y]

        # cost and gradients and learning rate
        lr = T.scalar('lr') # for SGD

        # NLL + L2-norm
        nll = -T.log(dropout_p_y_given_x)[y]
        cost = nll
        for param in self.params:
            if param.name == 'emb':
                continue
            cost += weight_decay * T.sum(param ** 2)

        # SGD
        if hyper_param['gradient_clip']:
            # exclude param from gradient cliping
            exclude_params = [self.emb]
            sgd_updates = myutil.sgd_updates_with_clipping(self.params, cost, lr, 9, exclude_params)
        else:
            gradients = T.grad(cost, self.params)
            sgd_updates = OrderedDict((p, p - lr*g) for p, g in zip(self.params, gradients))
        # SGD + momentum
        if hyper_param['gradient_clip']:
            momentum_updates = myutil.sgd_updates_momentum_with_clipping(self.params, cost, lr, 0.9, 9, exclude_params)
        else:
            momentum_updates = myutil.sgd_updates_momentum(self.params, cost, lr, 0.9)
        # AdaDelta (lr --> rho = 0.95)
        adadelta_updates = myutil.sgd_updates_adadelta(self.params, cost, lr, 1e-6, 9)

        # theano functions to compile
        self.classify = theano.function(inputs=[x_sentence, y], outputs=[y_pred, test_nll])
        self.classify2 = theano.function(inputs=[x_sentence, y], outputs=[y_pred, test_nll, a2])
        # SGD
        self.train_sgd = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=sgd_updates)
        # SGD with momentum
        self.train_momentum = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=momentum_updates)
        # AdaDelta
        self.train_adadelta = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=adadelta_updates)


