# -*- coding:utf-8 -*-
import cPickle
import gzip
import os
import random
import sys
import time
from operator import itemgetter

import grpc
import numpy

from maum.brain.nlp import nlp_pb2 as nlp
from maum.brain.nlp import nlp_pb2_grpc
from cltrainer.base_cl_proc import BaseClassifierTrainer
from cltrainer.kor import myutil
from cltrainer.kor.gru_encoder import GRU_encoder
from cltrainer.kor.gru_search import GRU_search
from cltrainer.kor.lstm_encoder import LSTM_encoder
from maum.brain.cl.train import cltrainer_pb2 as cltr


class KoreanClassifierTrainer(BaseClassifierTrainer):
    param = {
        # 'nn_type': 'GRU_encoder', # GRU based encoder-decoder model (identity init.)
        'nn_type': 'LSTM_encoder',
        # LSTM based encoder-decoder model (identity init.)
        # 'nn_type': 'GRU_search', # GRU based search model
        # 'activation': 'sigm', # sigmoid
        'activation': 'tanh',  # tanh
        # 'activation': 'relu', # ReLU
        # 'learning_method': 'sgd', # SGD
        'learning_method': 'momentum',  # SGD with momentum
        # 'learning_method': 'adadelta', # adadelta
        # 'lr': 0.001, # SGD, mommentum
        # 'lr': 0.002, # SGD, mommentum
        # 'lr': 0.005, # SGD, mommentum
        # 'lr': 0.01, # SGD, mommentum
        # 'lr': 0.02, # SGD, mommentum
        # 'lr': 0.05, # SGD, mommentum (tanh)
        # 'lr': 0.0015625, # SGD, mommentum (tanh)
        'lr': 0.1,  # SGD, mommentum (sigm)
        # 'lr': 0.98, # AdaDelta - rho
        # 'weight_decay': 0, # weight decay - by leeck
        # 'weight_decay': 1e-6, # weight decay - by leeck
        # 'weight_decay': 1e-5, # weight decay - by leeck
        'weight_decay': 1e-4,  # weight decay - by leeck
        # 'weight_decay': 1e-3, # weight decay - by leeck
        # 'dropout_rate': [0, 0], # dropout rate - by leeck
        'dropout_rate': [0.2, 0.5],
        # [0.2, 0.3], # dropout rate - by leeck
        #'nhidden': model.node_count,  # number of hidden units
        'nhidden2': 50,  # number of hidden units (Deep Output)
        'emb_dimension': 50,  # dimension of word embedding
        'fixed_emb': False,  # fixed embedding
        'gradient_clip': True,  # gradient clipping
        #'emb_file': self.base + 'kor_vector.nnlm.h50.bin',
        # name of word embedding file
        # 'emb_file': 'MR_data/embedding.txt', # name of word embedding file
        'load_model': '',
        # '../data/LSTM_encoder.tanh.momentum.h800.e50.d0.2-0.5.wd0.0001.best.pkl.gz', # '' --> build random parameters
        #'train_data': data_path + 'train.pkl.gz',
        # training data
        #'test_data': data_path + 'test.pkl.gz',  # test data
        # 'train_data': 'MR_data/MR.train.cv0.pkl.gz', # training data
        # 'test_data': 'MR_data/MR.test.cv0.pkl.gz', # test data
        #'folder': data_path,  # folder
        # 'folder': 'MR', # folder
        'begin_epoch': 0,
        #'nepochs': model.run_count,  # 50 is recommended
        'seed': 345,
        'skipsave': -1,  # don't save until 'skipsave' iterations
        'savenum': 1000000,  # save model per sentences
        'decay_lr_schedule': 3,
        # decay learning rate if the accuracy did not increase
        'decay_lr_rate': 0.9,
        # decay learning rate if the accuracy did not increase
        'savemodel': True
    }

    def __init__(self, helper, proc):
        BaseClassifierTrainer.__init__(self, helper, proc)
        nlp_remote = self.conf.get('brain-ta.cl.trainer.nlp.kor.remote')
        print("NLP_REMOTE is ", nlp_remote)
        nlp_chan = grpc.insecure_channel(nlp_remote)
        self.nlp_stub = nlp_pb2_grpc.NaturalLanguageProcessingServiceStub(nlp_chan)
        self.rsc = self.conf.get('brain-ta.resource.2.kor.path')
        if self.rsc[-1] != '/':
            self.rsc += '/'
        self.words_vocab = self.rsc + 'kor_vector.vocab'
        self.output = self.helper.workspace_path() + 'sa_model.txt'

    def encode_as(self, ustr):
        # return unicode(str, 'utf-8').encode('euc-kr')
        return ustr.encode('euc-kr')

    def read_data(self, category, vocab):
        """

        :param category:
        :param vocab:
        :return:
        """
        # data file
        print >> sys.stderr, 'Reading category lemmas', category.labels
        sentence_vec = []
        for tl in category.text_lemmas:
            uline = tl.lemma.replace('\r', '').replace('\n', '')
            line = self.encode_as(uline)
            word = line.split()
            if len(word) == 0: continue
            sentence = []
            for w in word:
                w = w.lower()
                if w in vocab:
                    sentence.append(vocab[w])
                else:
                    # update vocab
                    if True:
                        id = len(vocab)
                        vocab[w] = id
                        sentence.append(id)
                    else:
                        sentence.append(vocab['UNK'])
            sentence.append(vocab['</s>'])
            sentence_vec.append(sentence)
        print >> sys.stderr, "Done:", len(sentence_vec)
        return sentence_vec  # main

    def do_sa_data(self, uuid, model):
        base = self.base
        data_path = base + str(uuid)
        data_path += '/'
        if not os.path.isdir(data_path):
            os.makedirs(data_path)

        # get vocab(word,id) from word2vec
        self.proc.logs.append('kor_vector.vocab ...')
        vocab_word = self._load_vocab_word()
        self.proc.logs.append(' categories ...')
        vocab_label, list_label, src_list = self._load_vocab_label(model)

        # =======================================================
        train_sent_vec = []
        train_label_vec = []
        test_sent_vec = []
        test_sent_vec_org = []
        test_label_vec = []
        cv = 4
        cv_id = 0

        # read_vec = []
        j = -1
        for lb in list_label:
            j += 1
            cate_idx = src_list.index(lb)
            cate = model.categories[cate_idx]
            sent_line = []
            # category 에서 text만을 추출하여 배열로 만든다.
            for tl in cate.text_lemmas:
                sent_line.append(self.encode_as(tl.text))
            sent_org = []
            for sline in sent_line:
                sline = sline.replace('\n', '').replace('\r', '')
                if len(sline) == 0:
                    continue
                sent_org.append(sline)
            # category에서 lemma를 꺼내서 작업한다.
            sent_vec = self.read_data(cate, vocab_word)

            # 원래 코드는 크기가 같지 않으면 중지하는 것으로 되어 있다.
            assert len(sent_vec) == len(sent_org)

            for i, sent in enumerate(sent_vec):
                if i % cv == cv_id:
                    test_sent_vec.append(sent)
                    test_label_vec.append(j)
                    test_sent_vec_org.append(sent_org[i])
                else:
                    train_sent_vec.append(sent)
                    train_label_vec.append(j)
            ## 상태 저장
            self.proc.logs.append('made sa data label ')
            self.proc.value += 1
            self.helper.save_proc(self.proc)
        # read_vec.append(sent_vec)

        # pos_sent_vec = read_data(options.pos, vocab_word)
        # neg_sent_vec = read_data(options.neg, vocab_word)
        # =======================================================

        self.list_label = list_label
        ret = []
        f = open(data_path + 'kor_vector.new.vocab', 'wb')
        ret.append(f.name)
        list_word = sorted(vocab_word.iteritems(),
                           key=itemgetter(1),
                           reverse=False)
        for w in list_word:
            print >> f, w[0]
        f.close()
        self.proc.value += 1
        self.helper.save_proc(self.proc)

        # train_sent_vec = []
        # train_label_vec = []
        # test_sent_vec = []
        # test_label_vec = []
        # cv = 10
        # cv_id = 0
        # =======================================================
        # for j, read_f in read_vec:
        #     for i, sent in enumerate(read_f):
        #         if i % cv == cv_id:
        #             test_sent_vec.append(sent)
        #             test_label_vec.append(j)
        #         else:
        #             train_sent_vec.append(sent)
        #             train_label_vec.append(j)
        # for i, sent in enumerate(pos_sent_vec):
        #     if i % cv == cv_id:
        #         test_sent_vec.append(sent)
        #         test_label_vec.append(1)
        #     else:
        #         train_sent_vec.append(sent)
        #         train_label_vec.append(1)
        #
        # for i, sent in enumerate(neg_sent_vec):
        #     if i % cv == cv_id:
        #         test_sent_vec.append(sent)
        #         test_label_vec.append(0)
        #     else:
        #         train_sent_vec.append(sent)
        #         train_label_vec.append(0)
        # =======================================================

        f = gzip.open(os.path.join(data_path, 'train.pkl.gz'), 'wb')
        ret.append(f.name)
        cPickle.dump((train_sent_vec,
                      train_label_vec,
                      vocab_word,
                      vocab_label), f)
        f.close()
        self.proc.value += 1
        self.helper.save_proc(self.proc)

        f = gzip.open(os.path.join(data_path, 'test.pkl.gz'), 'wb')
        ret.append(f.name)
        cPickle.dump((test_sent_vec,
                      test_label_vec,
                      vocab_word,
                      vocab_label), f)
        f.close()
        self.proc.value += 1
        self.helper.save_proc(self.proc)

        f = open(os.path.join(data_path, 'test.org.txt'), 'w')
        ret.append(f.name)
        for s in test_sent_vec_org:
            f.write(s + '\n')
        f.close()
        self.proc.value += 1
        self.helper.save_proc(self.proc)

        return ret

    def do_dnn(self, uuid, model):
        self.proc.step = cltr.CL_TRAIN_DNN
        self.helper.save_proc(self.proc)

        model_name = self.encode_as(model.model)
        rsc = self.rsc
        base = self.base
        data_path = base + str(uuid)
        data_path += '/'

        param = self.param.copy()
        param['nhidden'] = model.node_count
        param['emb_file'] = rsc + 'kor_vector.nnlm.h50.bin'
        param['train_data'] = data_path + 'train.pkl.gz'
        # training data
        param['test_data'] = data_path + 'test.pkl.gz'
        param['folder'] = data_path
        param['nepochs'] = model.run_count

        print param

        # load the dataset
        f = gzip.open(param['train_data'])
        train_sent_vec, train_label_vec, vocab_word, vocab_label = \
            cPickle.load(f)
        f = gzip.open(param['test_data'])
        test_sent_vec, test_label_vec, vocab_word, vocab_label = \
            cPickle.load(f)
        print 'train_sent_vec size:', len(train_sent_vec), len(train_label_vec)
        print 'test_sent_vec size:', len(test_sent_vec), len(test_label_vec)
        print 'test_sent_vec[0]:', test_sent_vec[0]
        print 'test_label_vec[0]:', test_label_vec[0]
        train_x = train_sent_vec
        train_y = train_label_vec
        test_x = test_sent_vec
        test_y = test_label_vec
        dic = {}
        dic['labels2idx'] = vocab_label
        dic['words2idx'] = vocab_word
        folder = param['folder']

        if not os.path.exists(folder):
            os.makedirs(folder)

        idx2label = dict((k, v) for v, k in dic['labels2idx'].iteritems())
        idx2word = dict((k, v) for v, k in dic['words2idx'].iteritems())
        print 'Dic.'

        # import pdb; pdb.set_trace()

        # vocsize
        param['vocsize'] = len(dic['words2idx'])
        # nclasses
        param['nclasses'] = len(dic['labels2idx'])
        nsentences = len(train_x)

        print 'Size(voc,class,sent):', param['vocsize'], param[
            'nclasses'], nsentences

        groundtruth_test = [idx2label[y] for y in test_y]
        words_test = [map(lambda x: idx2word[x], w) for w in test_x]
        print 'ground truth data.'
        print 'words_test[0]:', words_test[0]
        print 'groundtruth_test[0]:', groundtruth_test[0]

        org_sents = open(data_path + 'test.org.txt', 'r').readlines()
        for i in range(len(org_sents)):
            org_sents[i] = org_sents[i].replace('\n', '').replace('\r', '')

        # instanciate the model
        numpy.random.seed(param['seed'])
        random.seed(param['seed'])

        if param['nn_type'] == 'GRU_encoder':
            nn = GRU_encoder(param, dic['words2idx'], dic['labels2idx'])
        elif param['nn_type'] == 'LSTM_encoder':
            nn = LSTM_encoder(param, dic['words2idx'], dic['labels2idx'])
        elif param['nn_type'] == 'GRU_search':
            nn = GRU_search(param, dic['words2idx'], dic['labels2idx'])
        else:
            print 'Error:', param['nn_type']
            sys.exit()
        print nn

        # train with early stopping on validation set
        best_test_ce = numpy.inf
        best_test_acc = 0
        start_time = time.time()
        no_acc_improvement_count = 0
        print 'training start.'
        for e in xrange(param['begin_epoch'], param['nepochs']):
            # shuffle
            myutil.shuffle([train_x, train_y], param['seed'])
            param['ce'] = e
            tic = time.time()
            tic2 = time.time()
            word_count = 0
            sent_count = 0
            sum_nll = 0

            # TODO, 아래에서도 상태를 업데이트 했으면 싶은데.. (몰라..)
            for i, (x, y) in enumerate(zip(train_x, train_y)):
                word_count += len(x)
                sent_count += 1
                [cost, nll] = nn.train(x, y, param['learning_method'],
                                       param['lr'])
                sum_nll += nll
                print '\r[%i] %d' % (e, i + 1),
                print 'CE=%.3f' % (sum_nll / sent_count),
                print '%.1f(word/s)' % (word_count / (time.time() - tic2)),
                print '%.1f(sent/s)' % (
                    (i % param['savenum'] + 1) / (time.time() - tic2)),
                print '%.1f(m) %.2f(h)' % (
                    (time.time() - tic2) / 60,
                    (time.time() - start_time) / 3600),
                sys.stdout.flush()
                # evaluation // back into the real world : idx -> words
                if (i + 1) % param['savenum'] == 0 or i + 1 == len(train_x):
                    sum_nll = 0
                    word_count = 0
                    sent_count = 0
                    # test set
                    tic_test = time.time()
                    test_word_count = 0
                    test_ce = 0
                    predictions_test = []
                    attentions_test = []
                    for (x, y) in zip(test_x, test_y):
                        if 'GRU_search' in param['nn_type']:
                            [y_pred, test_nll, attention] = nn.classify2(x, y)
                            attentions_test.append(attention)
                        else:
                            [y_pred, test_nll] = nn.classify(x, y)
                        # print 'y_pred:', y_pred.shape, y_pred
                        # print 'test_nll:', test_nll.shape, test_nll
                        test_word_count += 1
                        test_ce += float(test_nll)
                        predictions_test.append(idx2label[int(y_pred)])
                    # evaluation // CE
                    test_ce = test_ce / test_word_count
                    # evaluation // accuracy
                    test_acc = 0
                    for y_pred, y in zip(predictions_test, groundtruth_test):
                        if y_pred == y: test_acc += 1
                    test_acc = 100.0 * test_acc / len(predictions_test)
                    test_acc = float('%.2f' % test_acc)
                    print '\n(t: %.1f)' % (time.time() - tic_test),
                    sys.stdout.flush()

                    if test_acc > best_test_acc:
                        if param['savemodel'] and e > param['skipsave']:
                            tic_save = time.time()
                            self.best_file = nn.save(folder, param['nn_type'], e)
                            print '(s: %.1f)' % (time.time() - tic_save),
                            # write output
                            file_name = folder + '/predict.%s.%s.%s.%s.h%d.e%d.i%02d.txt' % (
                                model_name, param['nn_type'], param['activation'],
                                param['learning_method'], param['nhidden'],
                                param['emb_dimension'], e)
                            f = open(file_name, 'w')
                            if 'GRU_search' in param['nn_type']:
                                for y_pred, y, x, attention in zip(
                                        predictions_test,
                                        groundtruth_test,
                                        test_x,
                                        attentions_test):
                                    print >> f, y_pred, y
                                    print >> f, '#',
                                    for i in range(min(len(x), len(attention))):
                                        print >> f, idx2word[x[i]] + '/' + str(
                                                int(100 * attention[i])),
                                    print >> f
                            else:
                                for w, y_pred, y in zip(org_sents,
                                                        predictions_test,
                                                        groundtruth_test):
                                    str_check = 'False'
                                    if y_pred == y:
                                        str_check = 'True'
                                    f.write(model_name +
                                            '\t' + w +
                                            '\t' + y +
                                            '\t' + y_pred +
                                            '\t' + str_check + '\n')
                            f.close()
                        best_test_ce = test_ce
                        best_test_acc = test_acc
                        param['bte'] = e
                        no_acc_improvement_count = 0
                        print 'New BEST Test CE: %.3f Acc: %.2f' % (
                            test_ce, test_acc)
                    else:
                        no_acc_improvement_count += 1
                        print 'Test CE: %.3f Acc: %.2f' % (test_ce, test_acc)
                        if no_acc_improvement_count >= param[
                            'decay_lr_schedule']:
                            old_lr = param['lr']
                            #if param['lr'] == 0 :
                            #    break
                            param['lr'] *= param['decay_lr_rate']
                            print 'Learning rate: %f -> %f' % (
                            old_lr, param['lr'])
                            no_acc_improvement_count = 0
                    tic2 = time.time()

            self.proc.value += self.proc.node_count
            self.proc.run_cur += e
            self.helper.save_proc(self.proc)


        print(
        'BEST Test RESULT: epoch', param['bte'], 'best test CE', best_test_ce,
        'Acc', best_test_acc)
