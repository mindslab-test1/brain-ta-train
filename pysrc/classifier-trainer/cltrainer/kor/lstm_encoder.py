# -*- coding: utf-8 -*-

import cPickle
import gzip
from collections import OrderedDict

import numpy
import theano
from theano import tensor as T

import gru_encoder
from cltrainer.kor import myutil


class LSTM_encoder(gru_encoder.GRU_encoder):
    ''' LSTM RNN based encoder + MLP model '''
    def build_param(self, hyper_param, word2idx_dic, label2idx_dic):
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        # parameters of the model
        embedding = self.load_embedding(hyper_param['emb_file'], word2idx_dic, ne, de)
        self.emb = theano.shared(name='emb', value=embedding)
        # Wg
        self.Wgx = theano.shared(name='Wgx', value=0.01 * numpy.random.randn(de, 4*nh).astype(theano.config.floatX))
        # Ug
        identity = self.identity_weight(nh, 4*nh)
        self.Ugh = theano.shared(name='Ugh', value=identity.astype(theano.config.floatX))
        # bg
        self.bg = theano.shared(name='bg', value=numpy.zeros(4*nh, dtype=theano.config.floatX))
        # others
        self.Wyc = theano.shared(name='Wyc', value=0.01 * numpy.random.randn(nh, nc).astype(theano.config.floatX))
        self.by = theano.shared(name='by', value=numpy.zeros(nc, dtype=theano.config.floatX))
        self.h0 = theano.shared(name='h0', value=numpy.zeros(nh, dtype=theano.config.floatX))
        self.cell0 = theano.shared(name='cell0', value=numpy.zeros(nh, dtype=theano.config.floatX))

    def load_param(self, hyper_param, model_name):
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        print 'loading previous model:', model_name, '...',
        f = gzip.open(model_name, 'rb')
        [numpy_names, numpy_params] = cPickle.load(f)
        f.close()
        for numpy_param, name in zip(numpy_params, numpy_names):
            print name,
            if name == 'emb': self.emb = theano.shared(name=name, value=numpy_param)
            elif name == 'Wgx': self.Wgx = theano.shared(name=name, value=numpy_param)
            elif name == 'Ugh': self.Ugh = theano.shared(name=name, value=numpy_param)
            elif name == 'Wyc': self.Wyc = theano.shared(name=name, value=numpy_param)
            elif name == 'bg': self.bg = theano.shared(name=name, value=numpy_param)
            elif name == 'by': self.by = theano.shared(name=name, value=numpy_param)
            elif name == 'h0': self.h0 = theano.shared(name=name, value=numpy_param)
            elif name == 'cell0': self.cell0 = theano.shared(name=name, value=numpy_param)
            else: print 'skip:', name
        print 'done.'

    def __init__(self, hyper_param, word2idx_dic, label2idx_dic):
        '''
        nh :: dimension of the hidden layer
        nc :: number of classes
        ne :: number of word embeddings in the vocabulary
        de :: dimension of the word embeddings
        nf :: number of feature
        nfe:: number of feature embeddings in the vocabulary - by leeck
        dfe:: dimension of the feature embeddings - by leeck
        cs :: word window context size
        emb_file :: word embedding file
        weight_decay :: weight decay
        dropout_rate :: dropout rate
        activation :: activation function: simg, tanh, relu
        word2idx_dic :: word to index dictionary
        label2idx_dic :: label to index dictionary
        '''
        self.hyper_param = hyper_param
        nh = hyper_param['nhidden']
        nc = hyper_param['nclasses']
        ne = hyper_param['vocsize']
        de = hyper_param['emb_dimension']
        weight_decay = hyper_param['weight_decay']
        dropout_rate = hyper_param['dropout_rate']
        activation = hyper_param['activation']
        learning_method = hyper_param['learning_method']
        # parameters of the model
        if hyper_param['load_model'] != '':
            self.load_param(hyper_param, hyper_param['load_model'])
        else:
            self.build_param(hyper_param, word2idx_dic, label2idx_dic)

        # parameters
        self.params = [self.emb, self.Wgx, self.Ugh, \
                self.Wyc, self.bg, self.by, self.h0, self.cell0]

        if hyper_param['fixed_emb']:
            print 'fixed embeddig.'
            self.params.remove(self.emb)

        # as many lines as words in the sentence
        x_sentence = T.ivector('x_sentence')
        x_org = self.emb[x_sentence].reshape((x_sentence.shape[0], de))
        x = x_org[:-1] # remove '</s>'

        y = T.iscalar('y') # labels

        # for scan
        def step(x_t, h_tm1, cell_tm1):
            #print 'i_t, f_t, o_t, and ccell_t are combined!'
            all_t = T.dot(x_t, self.Wgx) + T.dot(h_tm1, self.Ugh) + self.bg
            i_t = T.nnet.sigmoid(myutil.slice(all_t, 0, nh))
            f_t = T.nnet.sigmoid(myutil.slice(all_t, 1, nh))
            o_t = T.nnet.sigmoid(myutil.slice(all_t, 2, nh))
            ccell_t = T.tanh(myutil.slice(all_t, 3, nh))
            # cell_t
            cell_t = i_t * ccell_t + f_t * cell_tm1
            # h_t
            h_t = o_t * myutil.activation(activation, cell_t)
            return [h_t, cell_t]

        # dropout: ex. [0, 0.2, 0.5]
        # input layer dropout
        print "Projection layer dropout:", dropout_rate[0]
        dropout_x = myutil.dropout_from_layer(x, dropout_rate[0])
        # forward recurrent
        [h, cell], _ = theano.scan(fn=step,
                sequences=dropout_x,
                outputs_info=[self.h0, self.cell0],
                n_steps=dropout_x.shape[0])
        c = h[-1]
        # hidden layer dropout
        print "Hidden layer dropout:", dropout_rate[1]
        dropout_c = myutil.dropout_from_layer(c, dropout_rate[1])
        # score
        dropout_s = T.dot(dropout_c, self.Wyc) + self.by
        dropout_p_y_given_x = myutil.mysoftmax(dropout_s)

        # scaled version (for prediction)
        scaled_x = x * (1.0 - dropout_rate[0])
        # forward recurrent
        [h2, cell2], _ = theano.scan(fn=step,
                sequences=scaled_x,
                outputs_info=[self.h0, self.cell0],
                n_steps=scaled_x.shape[0])
        c2 = h2[-1]
        # hidden layer
        scaled_c = c2 * (1.0 - dropout_rate[1])
        # score
        s = T.dot(scaled_c, self.Wyc) + self.by
        p_y_given_x = myutil.mysoftmax(s)
        # prediction
        #y_pred = T.argmax(p_y_given_x, axis=1)
        y_pred = T.argmax(p_y_given_x)
        print 'y_pred:', y_pred.ndim
        test_nll = -T.log(p_y_given_x)[y]
        print 'test_nll:', test_nll.ndim

        # cost and gradients and learning rate
        lr = T.scalar('lr') # for SGD

        # NLL + L2-norm
        nll = -T.log(dropout_p_y_given_x)[y]
        cost = nll
        for param in self.params:
            if param.name == 'emb':
                continue
            cost += weight_decay * T.sum(param ** 2)

        # SGD
        if hyper_param['gradient_clip']:
            # exclude param from gradient cliping
            exclude_params = [self.emb]
            sgd_updates = myutil.sgd_updates_with_clipping(self.params, cost, lr, 9, exclude_params)
        else:
            gradients = T.grad(cost, self.params)
            sgd_updates = OrderedDict((p, p - lr*g) for p, g in zip(self.params, gradients))
        # SGD + momentum
        if hyper_param['gradient_clip']:
            momentum_updates = myutil.sgd_updates_momentum_with_clipping(self.params, cost, lr, 0.9, 9, exclude_params)
        else:
            momentum_updates = myutil.sgd_updates_momentum(self.params, cost, lr, 0.9)
        # AdaDelta (lr --> rho = 0.95)
        adadelta_updates = myutil.sgd_updates_adadelta(self.params, cost, lr, 1e-6, 9)

        # theano functions to compile
        self.classify = theano.function(inputs=[x_sentence, y], outputs=[y_pred, test_nll])
        # SGD
        self.train_sgd = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=sgd_updates)
        # SGD with momentum
        self.train_momentum = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=momentum_updates)
        # AdaDelta
        self.train_adadelta = theano.function(inputs=[x_sentence, y, lr], outputs=[cost, nll], updates=adadelta_updates)

    def train(self, x, y, learning_method, learning_rate):
        words = map(lambda x: numpy.asarray(x).astype('int32'), x)
        label = y
        # learning_method : sgd, momentum, adadelta
        if learning_method == 'sgd':
            [cost, nll] = self.train_sgd(words, label, learning_rate)
        elif learning_method == 'momentum':
            [cost, nll] = self.train_momentum(words, label, learning_rate)
        elif learning_method == 'adadelta':
            [cost, nll] = self.train_adadelta(words, label, learning_rate)
        return [cost, nll]

