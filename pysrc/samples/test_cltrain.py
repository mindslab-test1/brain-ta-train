#!/usr/bin/env python
# -*- coding:utf-8 -*-
import argparse
import os
import sys

import grpc
import time
from google.protobuf import json_format

exe_path = os.path.realpath(sys.argv[0])
bin_path = os.path.dirname(exe_path)
lib_path = os.path.realpath(bin_path + '/../lib/python')
sys.path.append(lib_path)

from maum.brain.cl.train import cltrainer_pb2
from maum.brain.cl.train import cltrainer_pb2_grpc
from maum.common import lang_pb2
from common.config import Config


class CltrainerClient:
    conf = Config()
    stub = None

    def __init__(self):
        remote = 'localhost:' + conf.get('brain-ta.cl.trainer.front.port')
        print remote
        channel = grpc.insecure_channel(remote)
        self.stub = cltrainer_pb2_grpc.ClassifierTrainerStub(channel)

    def open(self, model):
        key = self.stub.Open(model)
        self.key = key
        json_ret = json_format.MessageToJson(key, True)
        print json_ret

    def get_progress(self):
        stat = self.stub.GetProgress(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        print json_ret
        return stat

    def close(self):
        stat = self.stub.Close(self.key)
        json_ret = json_format.MessageToJson(stat, True)
        print json_ret
        return stat

def test_cltrainer(model):
    client = CltrainerClient()

    client.open(model)
    time.sleep(5)
    while True:
        try:
            proc = client.get_progress()
            if proc.result == cltrainer_pb2.training or proc.result == cltrainer_pb2.preparing:
                time.sleep(5)
            else:
                break
        except:
            print sys.exc_info()[0]
            break
    client.close()

def make_kor_model():
    model = cltrainer_pb2.ClModel()
    model.model = 'test1'
    model.lang = lang_pb2.kor
    model.node_count = 300
    model.run_count = 50
    model.has_lemma = False
    # TODO, specify call back url
    # model.callback_url = None
    cate1 = cltrainer_pb2.ClCategory()
    cate1.labels.extend(['cate1', 'cate2'])
    t1 = cltrainer_pb2.TextLemma()
    t1.text = '나는 너를 사랑한다.'
    t2 = cltrainer_pb2.TextLemma()
    t2.text = '나는 너를 좋아한다.'
    cate1.text_lemmas.extend([t1, t2])

    cate2 = cltrainer_pb2.ClCategory()
    cate2.labels.extend(['cate1', 'cate3'])
    t3 = cltrainer_pb2.TextLemma()
    t3.text = '나는 너를 미워한다.'
    t4 = cltrainer_pb2.TextLemma()
    t4.text = '나는 너를 증오한다.'
    cate2.text_lemmas.extend([t3, t4])

    model.categories.extend([cate1, cate2])
    return model

def make_eng_model():
    model = cltrainer_pb2.ClModel()
    model.model = 'engtest'
    model.lang = lang_pb2.eng
    model.node_count = 300
    model.run_count = 50
    model.has_lemma = False
    # TODO, specify call back url
    model.callback_url = ''
    cate1 = cltrainer_pb2.ClCategory()
    cate1.labels.extend(['cate1', 'cate2'])
    t1 = cltrainer_pb2.TextLemma()
    t1.text = 'i love you.'
    t2 = cltrainer_pb2.TextLemma()
    t2.text = 'i like you.'
    cate1.text_lemmas.extend([t1, t2])

    cate2 = cltrainer_pb2.ClCategory()
    cate2.labels.extend(['cate1', 'cate3'])
    t3 = cltrainer_pb2.TextLemma()
    t3.text = 'i hate you.'
    t4 = cltrainer_pb2.TextLemma()
    t4.text = 'i dislike you.'
    cate2.text_lemmas.extend([t3, t4])

    model.categories.extend([cate1, cate2])
    return model

def make_model(file, lang, model_name, run, node):
    model = cltrainer_pb2.ClModel()
    model.model = model_name
    assert isinstance(lang, object)
    model.lang = lang_pb2.LangCode.Value(lang)
    model.node_count = node
    model.run_count = run
    model.has_lemma = False
    model.callback_url = ''

    f = open(file, 'r')
    lines = f.readlines()
    f.close()

    dic = {}
    for line in lines:
        vecline = line.strip().split('\t')
        if (len(vecline) != 2):
            continue
        vecline[1] = vecline[1].replace(' ', '_')
        if vecline[1] not in dic:
            temp_vec = vecline[0].split()
            if len(temp_vec) == 1 and len(temp_vec[0]) == 1:
                continue
            else:
                dic[vecline[1]] = [vecline[0]]
        else:
            dic[vecline[1]].append(vecline[0])

    cate_arr = []

    keys = dic.keys()
    keys.sort()
    for key in keys:
        cate = cltrainer_pb2.ClCategory()
        cate.labels.extend([key])
        #print key, '-------------'
        for sent in dic[key]:
            txt_lemma = cltrainer_pb2.TextLemma()
            txt_lemma.text = sent
            #print key, sent
            cate.text_lemmas.extend([txt_lemma])
        cate_arr.append(cate)

    model.categories.extend(cate_arr)
    return model


if __name__ == '__main__':
    conf = Config()
    conf.init('brain-ta-train.conf')

    parser = argparse.ArgumentParser(
        description='classifier trainer client')
    parser.add_argument('-f', '--file',
                        nargs='?',
                        dest='file',
                        required=True,
                        help='Filename for trainer')
    parser.add_argument('-l', '--lang',
                        nargs='?',
                        dest='lang',
                        required=True,
                        help='Language (kor, eng)')
    parser.add_argument('-m', '--model',
                        nargs='?',
                        dest='model',
                        required=True,
                        help='model name')
    parser.add_argument('-r', '--run',
                        nargs='?',
                        type=int,
                        dest='run_count',
                        required=True,
                        help='Run Count, use 50')
    parser.add_argument('-n', '--node',
                        nargs='?',
                        type=int,
                        dest='node_count',
                        required=True,
                        help='Node Count, use 300')
    args = parser.parse_args()
    print 'ARGS', args.file, args.lang, args.model, args.node_count, args.run_count

    if os.path.exists(args.file) == False:
        print 'File', args.file, 'not exist'
        sys.exit(1)

    test_cltrainer(make_model(args.file,
                              args.lang,
                              args.model,
                              int(args.run_count),
                              int(args.node_count)))
